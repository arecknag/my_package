from my_package.util_module.utils import starify


def test_starify():
    assert starify('abc') == '*abc*'
